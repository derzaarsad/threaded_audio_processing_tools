// --------------------------------------------------------------------------
//  Copyright © 2015  Christian Steiger  -  All rights reserved
// --------------------------------------------------------------------------

#include "audio.h"

int audio_init(snd_pcm_t **handle, audio_param_t *param)
{
	int ret;
	snd_pcm_hw_params_t *hw_params;

	ret = snd_pcm_open(handle, param->name, param->stream, 0);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_open: %s\n", snd_strerror (ret));
		exit(1);
	}

	ret = snd_pcm_hw_params_malloc(&hw_params);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params_malloc: %s\n", snd_strerror(ret));
		exit(1);
	}

	ret = snd_pcm_hw_params_any(*handle, hw_params);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params_any: %s\n", snd_strerror(ret));
		exit(1);
	}

	ret = snd_pcm_hw_params_set_access(*handle, hw_params, param->access);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params_set_access: %s\n", snd_strerror(ret));
		exit(1);
	}

	ret = snd_pcm_hw_params_set_format(*handle, hw_params, param->format);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params_set_format: %s\n", snd_strerror(ret));
		exit(1);
	}
	ret = snd_pcm_hw_params_set_rate_near(*handle, hw_params, &param->rate, 0);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params_set_rate_near: %s\n", snd_strerror(ret));
		exit(1);
	}

	ret = snd_pcm_hw_params_set_channels(*handle, hw_params, param->channel);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params_set_channels: %s\n", snd_strerror(ret));
		exit(1);
	}

	ret = snd_pcm_hw_params(*handle, hw_params);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params: %s\n", snd_strerror(ret));
		exit(1);
	}

	ret = snd_pcm_hw_params_get_period_size(hw_params, &param->period_size, 0);
	if (ret < 0) {
		fprintf(stderr, "snd_pcm_hw_params_get_period_size: %s\n", snd_strerror(ret));
		exit(1);
	}

	snd_pcm_hw_params_free(hw_params);

	return 0;
}
