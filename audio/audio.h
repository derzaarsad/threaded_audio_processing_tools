// --------------------------------------------------------------------------
//  Copyright © 2015  Christian Steiger  -  All rights reserved
// --------------------------------------------------------------------------

#ifndef INFOTECH_AUFGABE_1_BLOCK_AUDIO_H_
#define INFOTECH_AUFGABE_1_BLOCK_AUDIO_H_ 1

#include <alsa/asoundlib.h>

struct audio_param
{
	       const char *name;
	 snd_pcm_stream_t  stream;
	 snd_pcm_access_t  access;
	 snd_pcm_format_t  format;
	     unsigned int  channel;
	     unsigned int  rate;
	snd_pcm_uframes_t  period_size;
};

typedef struct audio_param audio_param_t;

int audio_init(snd_pcm_t **handle, audio_param_t *param);


#endif
