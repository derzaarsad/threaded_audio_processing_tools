// --------------------------------------------------------------------------
//  Copyright © 2015  Christian Steiger  -  All rights reserved
// J.Wietzke 26.3.2015
// --------------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <sched.h>
#include <alsa/asoundlib.h>
#include <math.h>      // tanh
#include <sys/types.h>
#include <string>

#include "audio/audio.h"

#include "visual/global.h"
#include "visual/gles.h"
#include "visual/draw.h"

#include "threading/threading.h"

void SineGenerator(float *bufptr)
{
    for(int i = 0; i < BUFFER_SIZE; i++) bufptr[i] = 0.15*sin(1000 * (2 * 3.14) * i / SAMPLE_RATE);
}

void find_argument(const int argc, const char **argv, bool &crybaby_used, bool &sine_used)
{
    for (int i= 0; i < argc; i++)
    {
        std::string argument = argv[i];
        if (argument == "-crybaby")
        {
            crybaby_used = true;
            std::cout << "crybaby used" << std::endl;
        }
        if (argument == "-sine")
        {
            sine_used = true;
            std::cout << "sine input used" << std::endl;
        }
    }
}

int main(int argc, const char * argv[])
{
    bool crybaby_used = false;
    bool sine_used = false;
    if (argc > 1) find_argument(argc, argv, crybaby_used, sine_used);

    float *bufptr = new float[BUFFER_SIZE];
    ThreadedProcess threadedProcess;
    threadedProcess.crybaby_used = crybaby_used;
    threadedProcess.start();


	//int ret;


	//----Gerüst für Capturing ---------------------------


	snd_pcm_t *capture;
    size_t blocksize = BUFFER_SIZE;										// Anzahl samples pro block
	size_t fsize = 4;											// byte pro sample

	//----Gerüst für graphics ---------------------------

	// ---- prepare graphics buffer ------------------------------------------------
	draw_data_t draw_data;
    draw_data.bufptr = threadedProcess.bufout;								// ausimplementieren, Zeiger auf anzuzeigenden Block
	draw_data.blocksize = blocksize;						// ausimplementieren, Grüße des Blocks
    draw_data.isDestroyed = false;

	// -----prepare & start graphics						// parallel thread erzeugen und starten
	pthread_t draw_thread;
	pthread_create(&draw_thread,  0, draw_main, &draw_data);


	// ------------------------------------------------------------------------------------------


	// ---- Capture Parameter ----------------------------------------------------
	audio_param_t  param_in;
	param_in.name = "default";
	param_in.stream = SND_PCM_STREAM_CAPTURE;
	param_in.access = SND_PCM_ACCESS_RW_INTERLEAVED;
	param_in.format = SND_PCM_FORMAT_FLOAT_LE;
	param_in.channel = 1;									// mono
    param_in.rate = SAMPLE_RATE;								// Abtastrate

	// Capture
	audio_init(&capture, &param_in);						// start Capturing

	// Number of frames to record each step


	// Frame size
	// allocate buffer for one block   !!



	// ---- Capture ----------------------------------------------------
	int ret;
	while (1)
	{
        if(sine_used == true) SineGenerator(bufptr);
        else ret = snd_pcm_readi(capture, bufptr, blocksize);	// capture one block


        threadedProcess.catchBuf(bufptr);

        if (draw_data.isDestroyed == true)
            break;
	}




    threadedProcess.stop();
	ret = snd_pcm_close(capture);						// Audio capturing schliessen
	printf ("capture done \n");



	// vor Ende auf draw thread warten

	printf("fin \n");
	pthread_join(draw_thread, 0);

	return 0;
}
