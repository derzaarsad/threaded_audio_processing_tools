#include "crybaby.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <math.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multiroots.h>

#include "dampednewton/dampednewton.h"

#define SOLVE_FOR_V 1

#define CRYBABY_URI       "http://ant.hsu-hh.de/audio/crybaby";
#define PORT_INPUT     0
#define PORT_OUTPUT    1
#define PORT_REFVOLT   2
#define PORT_HOTPOTZ   3 
#define MAX_DELTA 0.001

#define NUM_NODES 13
#define NUM_VOLTAGE_SOURCES 2
#define NUM_POTENTIOMETERS 1
#define NUM_CAPACITORS 5
#define NUM_INDUCTORS 1
#define NUM_STATES (NUM_INDUCTORS + NUM_CAPACITORS)
#define NUM_NONLINEARS 4

#define SYSTEM_MATRIX_SIZE (NUM_NODES + NUM_VOLTAGE_SOURCES)

#define SUPPLY_VOLTAGE 8.15
#define INPUT_TAU 0.012

//static LV2_Descriptor *crybabyDescriptor = NULL; //not important

/* definition of the circuit elements as structs with their value and the nodes
 * they are conencted to */
namespace CrybabySpace {

static const struct {
  float value;
  unsigned int node1;
  unsigned int node2;
} resistors[] = {
  { 68e3,  1,  2},
  { 22e3, 13,  4},
  {390  ,  5,  0},
  {470e3,  4,  6},
  {470e3,  4, 10},
  {1.5e3,  3,  7},
  { 33e3,  6,  7},
  { 82e3,  6,  0},
  { 10e3, 12,  0},
  {  1e3, 13, 11},
  {0}
};

static const struct {
  float value;
  unsigned int node1;
  unsigned int node2;
  unsigned int node3;
} potentiometers[NUM_POTENTIOMETERS + 1] = {
  {100e3,  8,  9,  0},
  {0}
};

static const struct {
  float value;
  unsigned int node1;
  unsigned int node2;
} capacitors[NUM_CAPACITORS + 1] = {
  { 10e-9,  3,  2},
  {4.7e-6,  6,  0},
  { 10e-9,  7, 12},
  {220e-9,  4,  8},
  {220e-9,  9, 10},
  {0}
};

static const struct {
  float value;
  unsigned int node1;
  unsigned int node2;
} inductors[NUM_INDUCTORS + 1] = {
  {500e-3,  6,  7},
  {0}
};

static const struct {
  unsigned int node1;
  unsigned int node2;
} voltage_sources[NUM_VOLTAGE_SOURCES + 1] = {
  {1, 0},
  {13, 0},
  {0}
};

static const struct {
  unsigned int node1;
  unsigned int node2;
} output_voltage = { 8,  0};

static const struct {
  unsigned int node1;
  unsigned int node2;
} nonlinear_elements[] = {
  { 4,  3},
  { 4,  5},
  {11, 10},
  {11, 12},
  {0}
};

static void update_coefficients (Crybaby *plugin_data);
static void initialize_state (Crybaby *plugin_data);
static int nonlinear_equation_f (const gsl_vector *v, void *params,
				 gsl_vector *res);
static int nonlinear_equation_df (const gsl_vector *v, void *params,
				  gsl_matrix *J);
static int nonlinear_equation_fdf (const gsl_vector *v, void *params,
				   gsl_vector *res, gsl_matrix *J);
static void ebers_moll (double vCB, double vCE, gsl_vector *i, gsl_matrix *J);

void cleanup(Crybaby *instance)
{
  Crybaby *plugin_data = instance;
  gsl_matrix_free (plugin_data->S0);
  gsl_matrix_free (plugin_data->ABC_DEF_GHK0);
  gsl_vector_free (plugin_data->v);

  gsl_matrix_free (plugin_data->RvQ);
  gsl_matrix_free (plugin_data->RvQinv);
  gsl_matrix_free (plugin_data->RvQinvU);
  gsl_matrix_free (plugin_data->ABC_DEF_GHK);
  gsl_vector_free (plugin_data->xui);
  gsl_vector_free (plugin_data->p);
  gsl_vector_free (plugin_data->x_new_y);
  gsl_matrix_free (plugin_data->S0inv);
  gsl_matrix_free (plugin_data->Q);
  gsl_matrix_free (plugin_data->Uleft);
  gsl_matrix_free (plugin_data->Uxun);

  gsl_permutation_free (plugin_data->RvQperm);

  gsl_multiroot_fdfsolver_free (plugin_data->solver);

  free(instance);
}

void connectPort(Crybaby *instance, float*inputptr, float*outputptr, float*refVolt, float*hotPotz)
{
	//all default
  Crybaby *plugin = instance;

  /*switch (port) {
    case PORT_INPUT:*/
      plugin->input = inputptr;//data;
      //break;
    //case PORT_OUTPUT:
      plugin->output = outputptr;//data;
      //break;
    //case PORT_REFVOLT:
      plugin->refvolt = refVolt;//data;
      //break;
    //case PORT_HOTPOTZ:
      plugin->hotpotz = hotPotz;//data;
      //break;
  //}
}

void addHotPotz(Crybaby *instance, float val)
{
  Crybaby *plugin = instance;


  *plugin->hotpotz += val;//data;
  if(*plugin->hotpotz > 1.0)
	  *plugin->hotpotz = 0.0;

  /* initialize potentiometer dependent coefficients */
  update_coefficients (plugin);

  /* reset state */
  initialize_state (plugin);
}

//pretty print not important
/*static void
pretty_print_matrix (const char *name, const gsl_matrix *M)
{
  unsigned int row, col;
  printf ("%s =\n\n", name);
  for (row = 0; row < M->size1; row++) {
    for (col = 0; col < M->size2; col++) {
      printf ("  %11.4e", gsl_matrix_get (M, row, col));
    }
    puts("");
  }
  puts("");
}

static void
pretty_print_vector (const char *name, const gsl_vector *v)
{
  unsigned int row;
  printf ("%s =\n\n", name);
  for (row = 0; row < v->size; row++) {
    printf ("  %11.4e", gsl_vector_get (v, row));
  }
  puts("");
}*/

static void
add_element_to_system_matrix (gsl_matrix *S, float coductance, unsigned int node1, 
			      unsigned int node2)
{
  if (node1 != 0)
    *gsl_matrix_ptr (S, node1 - 1, node1 - 1) += coductance;
  if (node2 != 0)
    *gsl_matrix_ptr (S, node2 - 1, node2 - 1) += coductance;
  if (node1 != 0 && node2 != 0) {
    *gsl_matrix_ptr (S, node1 - 1, node2 - 1) -= coductance;
    *gsl_matrix_ptr (S, node2 - 1, node1 - 1) -= coductance;
  }
}

Crybaby *instantiate (double s_rate)
{
  unsigned int n;
  Crybaby *plugin_data = (Crybaby *)malloc(sizeof(Crybaby));

  /* calculate input high-pass from time constant tau */
  plugin_data->input_a
    = (1. - 2. * INPUT_TAU * s_rate) / (1. + 2. * INPUT_TAU * s_rate);
  plugin_data->input_b
    = 2. * INPUT_TAU * s_rate / (1. + 2. * INPUT_TAU * s_rate);
  plugin_data->input_state = 0.;

  /* pre-allocate all the matrices needed */
  plugin_data->S0 = gsl_matrix_calloc (SYSTEM_MATRIX_SIZE, SYSTEM_MATRIX_SIZE);
  plugin_data->ABC_DEF_GHK0 = gsl_matrix_calloc (NUM_STATES + 1 + NUM_NONLINEARS, NUM_STATES + NUM_VOLTAGE_SOURCES + NUM_NONLINEARS);
  plugin_data->v = gsl_vector_calloc (NUM_NONLINEARS);

  gsl_matrix_view A0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, 0, 0, NUM_STATES, NUM_STATES);
  gsl_matrix_view B0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, 0, NUM_STATES, NUM_STATES, NUM_VOLTAGE_SOURCES);
  gsl_matrix_view C0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, 0, NUM_STATES + NUM_VOLTAGE_SOURCES, NUM_STATES, NUM_NONLINEARS);
  gsl_matrix_view D0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, NUM_STATES, 0, 1, NUM_STATES);
  gsl_matrix_view E0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, NUM_STATES, NUM_STATES, 1, NUM_VOLTAGE_SOURCES);
  gsl_matrix_view F0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, NUM_STATES, NUM_STATES + NUM_VOLTAGE_SOURCES, 1, NUM_NONLINEARS);
  gsl_matrix_view G0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, NUM_STATES + 1, 0, NUM_NONLINEARS, NUM_STATES);
  gsl_matrix_view H0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, NUM_STATES + 1, NUM_STATES, NUM_NONLINEARS, NUM_VOLTAGE_SOURCES);
  gsl_matrix_view K0view = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK0, NUM_STATES + 1, NUM_STATES + NUM_VOLTAGE_SOURCES, NUM_NONLINEARS, NUM_NONLINEARS);

  plugin_data->RvQ = gsl_matrix_calloc (2 * NUM_POTENTIOMETERS, 2 * NUM_POTENTIOMETERS);
  plugin_data->RvQperm = gsl_permutation_calloc (2 * NUM_POTENTIOMETERS);
  plugin_data->RvQinv = gsl_matrix_calloc (2 * NUM_POTENTIOMETERS, 2 * NUM_POTENTIOMETERS);
  plugin_data->RvQinvU = gsl_matrix_calloc (2 * NUM_POTENTIOMETERS, NUM_STATES + NUM_VOLTAGE_SOURCES + NUM_NONLINEARS);
  plugin_data->ABC_DEF_GHK = gsl_matrix_alloc (NUM_STATES + 1 + NUM_NONLINEARS,
                                               NUM_STATES + NUM_VOLTAGE_SOURCES + NUM_NONLINEARS);
  plugin_data->Kview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, NUM_STATES + 1, NUM_STATES + NUM_VOLTAGE_SOURCES, NUM_NONLINEARS, NUM_NONLINEARS);
  plugin_data->ABC_DEFview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, 0, 0, NUM_STATES + 1, NUM_STATES + NUM_VOLTAGE_SOURCES + NUM_NONLINEARS);
  plugin_data->GHview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, NUM_STATES + 1, 0, NUM_NONLINEARS, NUM_STATES + NUM_VOLTAGE_SOURCES);
  plugin_data->xui = gsl_vector_alloc (NUM_STATES + NUM_VOLTAGE_SOURCES + NUM_NONLINEARS);
  plugin_data->xview = gsl_vector_subvector (plugin_data->xui, 0, NUM_STATES);
  plugin_data->uview = gsl_vector_subvector (plugin_data->xui, NUM_STATES, NUM_VOLTAGE_SOURCES);
  plugin_data->xuview = gsl_vector_subvector (plugin_data->xui, 0, NUM_STATES + NUM_VOLTAGE_SOURCES);
  plugin_data->iview = gsl_vector_subvector (plugin_data->xui, NUM_STATES + NUM_VOLTAGE_SOURCES, NUM_NONLINEARS);
  plugin_data->p = gsl_vector_alloc (NUM_NONLINEARS);
  plugin_data->x_new_y = gsl_vector_alloc (NUM_STATES + 1);
  plugin_data->x_newview = gsl_vector_subvector (plugin_data->x_new_y, 0, NUM_STATES);
  plugin_data->yview = gsl_vector_subvector (plugin_data->x_new_y, NUM_STATES, 1);

  /* setup non-linear solver */
  plugin_data->solver
    = gsl_multiroot_fdfsolver_alloc (multiroot_fdfsolver_damped_newton, 
				     NUM_NONLINEARS);
  plugin_data->nonlinear_system.f = nonlinear_equation_f;
  plugin_data->nonlinear_system.df = nonlinear_equation_df;
  plugin_data->nonlinear_system.fdf = nonlinear_equation_fdf;
  plugin_data->nonlinear_system.n = NUM_NONLINEARS;

  /* allocate coincidence matrices */
  gsl_matrix *Nx = gsl_matrix_calloc (NUM_STATES, SYSTEM_MATRIX_SIZE);
  gsl_matrix *ZGNx = gsl_matrix_calloc (NUM_STATES, SYSTEM_MATRIX_SIZE);
  gsl_matrix *Nv = gsl_matrix_calloc (2 * NUM_POTENTIOMETERS, SYSTEM_MATRIX_SIZE);
  gsl_matrix *No = gsl_matrix_calloc (1, SYSTEM_MATRIX_SIZE);
  gsl_matrix *Nn = gsl_matrix_calloc (NUM_NONLINEARS, SYSTEM_MATRIX_SIZE);

  /* fill in system matrix and coincidence matrices */
  for (n = 0; resistors[n].value != 0; n++) {
    float coductance = 1 / resistors[n].value;
    unsigned int node1 = resistors[n].node1;
    unsigned int node2 = resistors[n].node2;
    add_element_to_system_matrix (plugin_data->S0, coductance, node1, node2);
  }
  for (n = 0; capacitors[n].value != 0; n++) {
    float conductance = 2 * capacitors[n].value * s_rate;
    unsigned int node1 = capacitors[n].node1;
    unsigned int node2 = capacitors[n].node2;
    add_element_to_system_matrix (plugin_data->S0, conductance, node1, node2);
    if (node1 != 0) {
      gsl_matrix_set (Nx, n, node1 - 1, 1);
      gsl_matrix_set (ZGNx, n, node1 - 1, 2 * conductance);
    }
    if (node2 != 0) {
      gsl_matrix_set (Nx, n, node2 - 1, -1);
      gsl_matrix_set (ZGNx, n, node2 - 1, -2 * conductance);
    }
    gsl_matrix_set (&A0view.matrix, n, n, -1);
  }
  for (n = 0; inductors[n].value != 0; n++) {
    float coductance = 1. / (2 * inductors[n].value * s_rate);
    unsigned int node1 = inductors[n].node1;
    unsigned int node2 = inductors[n].node2;
    add_element_to_system_matrix (plugin_data->S0, coductance, node1, node2);
    if (node1 != 0) {
      gsl_matrix_set (Nx, n + NUM_CAPACITORS, node1 - 1, 1);
      gsl_matrix_set (ZGNx, n + NUM_CAPACITORS, node1 - 1, -2 * coductance);
    }
    if (node2 != 0) {
      gsl_matrix_set (Nx, n + NUM_CAPACITORS, node2 - 1, -1);
      gsl_matrix_set (ZGNx, n + NUM_CAPACITORS, node2 - 1, 2 * coductance);
    }
    gsl_matrix_set (&A0view.matrix, n + NUM_CAPACITORS, n + NUM_CAPACITORS, 1);
  }
  for (n = 0; voltage_sources[n].node1 != 0 || voltage_sources[n].node2 != 0; 
       n++) {
    unsigned int node1 = voltage_sources[n].node1;
    unsigned int node2 = voltage_sources[n].node2;
    if (node1 != 0) {
      *gsl_matrix_ptr (plugin_data->S0, NUM_NODES + n, node1 - 1) = 1;
      *gsl_matrix_ptr (plugin_data->S0, node1 - 1, NUM_NODES + n) = 1;
    }
    if (node2 != 0) {
      *gsl_matrix_ptr (plugin_data->S0, NUM_NODES + n, node2 - 1) = 1;
      *gsl_matrix_ptr (plugin_data->S0, node2 - 1, NUM_NODES + n) = 1;
    }
  }
  for (n = 0; nonlinear_elements[n].node1 != 0 && nonlinear_elements[n].node2 != 0; n++) {
    unsigned int node1 = nonlinear_elements[n].node1;
    unsigned int node2 = nonlinear_elements[n].node2;
    if (node1 != 0)
      gsl_matrix_set (Nn, n, node1 - 1, 1);
    if (node2 != 0)
      gsl_matrix_set (Nn, n, node2 - 1, -1);
  }
  if (output_voltage.node1 != 0)
    gsl_matrix_set (No, 0, output_voltage.node1 - 1, 1);
  if (output_voltage.node2 != 0)
    gsl_matrix_set (No, 0, output_voltage.node2 - 1, -1);

  for (n = 0; potentiometers[n].value != 0; n++) {
    unsigned int node1 = potentiometers[n].node1;
    unsigned int node2 = potentiometers[n].node2;
    unsigned int node3 = potentiometers[n].node3;
    if (node1 != 0)
      gsl_matrix_set (Nv, 2*n, node1 - 1, 1);
    if (node2 != 0) {
      gsl_matrix_set (Nv, 2*n, node2 - 1, -1);
      gsl_matrix_set (Nv, 2*n + 1, node2 - 1, 1);
    }
    if (node3 != 0)
      gsl_matrix_set (Nv, 2*n + 1, node3 - 1, -1);
  }

  /* invert system matrix */
  int signum;
  gsl_permutation *p = gsl_permutation_calloc (SYSTEM_MATRIX_SIZE);
  plugin_data->S0inv = gsl_matrix_alloc (SYSTEM_MATRIX_SIZE, SYSTEM_MATRIX_SIZE);
  gsl_linalg_LU_decomp (plugin_data->S0, p, &signum);
  gsl_linalg_LU_invert (plugin_data->S0, p, plugin_data->S0inv);
  gsl_permutation_free (p);

  /* calculate S0^-1 * Nv */
  gsl_matrix *S0invNv = gsl_matrix_calloc (SYSTEM_MATRIX_SIZE, 2 * NUM_POTENTIOMETERS);
  plugin_data->Q = gsl_matrix_calloc (2 * NUM_POTENTIOMETERS, 2 * NUM_POTENTIOMETERS);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1, plugin_data->S0inv, Nv, 0, S0invNv);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, Nv, S0invNv, 0, plugin_data->Q);

  /* calculate Ux and 2 * Z * Gx * Ux */
  plugin_data->Uxun = gsl_matrix_calloc (NUM_STATES + NUM_NONLINEARS + NUM_VOLTAGE_SOURCES, 2 * NUM_POTENTIOMETERS);
  gsl_matrix_view Uxview = gsl_matrix_submatrix (plugin_data->Uxun, 0, 0, NUM_STATES, 2 * NUM_POTENTIOMETERS);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, Nx, S0invNv, 0, &Uxview.matrix);
  plugin_data->Uleft = gsl_matrix_alloc (NUM_STATES + 1 + NUM_NONLINEARS, 2 * NUM_POTENTIOMETERS);
  gsl_matrix_view ZGUxview = gsl_matrix_submatrix (plugin_data->Uleft, 0, 0, NUM_STATES, 2 * NUM_POTENTIOMETERS);
  gsl_matrix_memcpy (&ZGUxview.matrix, &Uxview.matrix);
  for (n = 0; capacitors[n].value != 0; n++) {
    float conductance = 2 * capacitors[n].value * s_rate;
    gsl_vector_view row = gsl_matrix_row (&ZGUxview.matrix, n);
    gsl_vector_scale (&row.vector, 2 * conductance);
  }
  for (n = 0; inductors[n].value != 0; n++) {
    float conductance = 1. / (2 * inductors[n].value * s_rate);
    gsl_vector_view row = gsl_matrix_row (&ZGUxview.matrix, n + NUM_CAPACITORS);
    gsl_vector_scale (&row.vector, -2 * conductance);
  }

  /* calculate Uo */
  gsl_matrix_view Uoview = gsl_matrix_submatrix (plugin_data->Uleft, NUM_STATES, 0, 1, 2 * NUM_POTENTIOMETERS);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, No, S0invNv, 0, &Uoview.matrix);

  /* calculate Un */
  gsl_matrix_view Unview = gsl_matrix_submatrix (plugin_data->Uxun, NUM_STATES + NUM_VOLTAGE_SOURCES, 0, NUM_NONLINEARS , 2 * NUM_POTENTIOMETERS);
  gsl_matrix_view Unview2 = gsl_matrix_submatrix (plugin_data->Uleft, NUM_STATES + 1, 0, NUM_NONLINEARS, 2 * NUM_POTENTIOMETERS);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, Nn, S0invNv, 0, &Unview.matrix);
  gsl_matrix_memcpy (&Unview2.matrix, &Unview.matrix);

  /* extract Uu */
  gsl_matrix_view Uuview = gsl_matrix_submatrix (plugin_data->Uxun, NUM_STATES, 0, NUM_VOLTAGE_SOURCES , 2 * NUM_POTENTIOMETERS);
  gsl_matrix_view mv = gsl_matrix_submatrix (S0invNv, NUM_NODES, 0, NUM_VOLTAGE_SOURCES, 2 * NUM_POTENTIOMETERS);
  gsl_matrix_memcpy (&Uuview.matrix, &(mv.matrix));

  /* calculate S0^-1 * Nx and S0^-1 * Nn */
  gsl_matrix *S0invNx = gsl_matrix_calloc (SYSTEM_MATRIX_SIZE, NUM_STATES);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1, plugin_data->S0inv, Nx, 0, S0invNx);
  gsl_matrix *S0invNn = gsl_matrix_calloc (SYSTEM_MATRIX_SIZE, NUM_NONLINEARS);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1, plugin_data->S0inv, Nn, 0, S0invNn);

  /* using the above, calculate A0, ..., K0 */
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, ZGNx, S0invNx, 1, &A0view.matrix);

  mv = gsl_matrix_submatrix (plugin_data->S0inv, 0, NUM_NODES, SYSTEM_MATRIX_SIZE, NUM_VOLTAGE_SOURCES);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, ZGNx, &mv.matrix, 0, &B0view.matrix);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, ZGNx, S0invNn, 0, &C0view.matrix);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, No, S0invNx, 0, &D0view.matrix);

  mv = gsl_matrix_submatrix (plugin_data->S0inv, 0, NUM_NODES, SYSTEM_MATRIX_SIZE, NUM_VOLTAGE_SOURCES);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, No, &mv.matrix, 0, &E0view.matrix);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, No, S0invNn, 0, &F0view.matrix);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, Nn, S0invNx, 0, &G0view.matrix);

  mv = gsl_matrix_submatrix (plugin_data->S0inv, 0, NUM_NODES, SYSTEM_MATRIX_SIZE, NUM_VOLTAGE_SOURCES);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, Nn, &mv.matrix, 0, &H0view.matrix);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, Nn, S0invNn, 0, &K0view.matrix);

  /* calculation of constant coefficients done, clean up temporaries */
  gsl_matrix_free (Nx);
  gsl_matrix_free (ZGNx);
  gsl_matrix_free (Nv);
  gsl_matrix_free (No);
  gsl_matrix_free (Nn);
  gsl_matrix_free (S0invNv);
  gsl_matrix_free (S0invNx);
  gsl_matrix_free (S0invNn);

  plugin_data->sampling_rate = s_rate;

  plugin_data->current_hotpotz = 0.5;

  plugin_data->nl_params.Jt = gsl_matrix_calloc (4, 4);
#if SOLVE_FOR_V
  plugin_data->nl_params.i = gsl_vector_alloc (4);
#else
  plugin_data->nl_params.v = gsl_vector_calloc (4);
#endif

  /* initialize potentiometer dependent coefficients */
  update_coefficients (plugin_data);

  /* reset state */
  initialize_state (plugin_data);

  return plugin_data;
}

void run (Crybaby* instance, uint32_t sample_count)
{
  Crybaby *plugin_data = instance;

  const float * const input = plugin_data->input;
  float * const output = plugin_data->output;
  float const refvolt = *(plugin_data->refvolt);

  uint32_t pos;

  /* second input is constant supply voltage */
  gsl_vector_set (&plugin_data->uview.vector, 1, SUPPLY_VOLTAGE);

  /* setup parameters for the non-linear solver */
  plugin_data->nl_params.K = &plugin_data->Kview.matrix;
  plugin_data->nl_params.p = plugin_data->p;
  plugin_data->nonlinear_system.params = &plugin_data->nl_params;

  for (pos = 0; pos < sample_count; pos++) {
    gsl_vector_view vv;
    double input_temp;

    /* apply input high-pass and store result as first input voltage */
    input_temp = plugin_data->input_b * refvolt * input[pos] 
      - plugin_data->input_a * plugin_data->input_state;
    gsl_vector_set (&plugin_data->uview.vector, 0, input_temp - plugin_data->input_state);
    plugin_data->input_state = input_temp;

    /* the potentiometer value is smoothed; setting it only once per frame
     * otherwise would mean relatively large jumps, resulting in audible clicks
     */
    if (*plugin_data->hotpotz != plugin_data->current_hotpotz) {
      if (*plugin_data->hotpotz > plugin_data->current_hotpotz + MAX_DELTA)
	plugin_data->current_hotpotz += MAX_DELTA;
      else if (*plugin_data->hotpotz < plugin_data->current_hotpotz - MAX_DELTA)
	plugin_data->current_hotpotz -= MAX_DELTA;
      else
	plugin_data->current_hotpotz = *plugin_data->hotpotz;
      update_coefficients (plugin_data);
    }

    /* p = G * x + H * u */
    gsl_blas_dgemv (CblasNoTrans, 1, &plugin_data->GHview.matrix, &plugin_data->xuview.vector, 0, plugin_data->p);

    unsigned int iter = 0;
    /* solve non-linearity to obtain i */
#if SOLVE_FOR_V
    gsl_multiroot_fdfsolver_set (plugin_data->solver, &plugin_data->nonlinear_system, plugin_data->v);
    do {
      gsl_multiroot_fdfsolver_iterate (plugin_data->solver);
    } while (iter++ < 10 && gsl_blas_dnrm2 (gsl_multiroot_fdfsolver_f (plugin_data->solver)) > 1e-5);
    gsl_vector_memcpy (plugin_data->v, gsl_multiroot_fdfsolver_root (plugin_data->solver));
/*    for (int i = 0; i < NUM_NONLINEARS; i++)   //OUT OF RANGE ERROR
        printf ("v(%d) = %g\n", i,
                gsl_vector_get (plugin_data->v, i));*/

    vv = gsl_vector_subvector (&plugin_data->iview.vector, 0, 2);
    ebers_moll (gsl_vector_get (plugin_data->v, 0), gsl_vector_get (plugin_data->v, 1), &vv.vector, NULL);
    vv = gsl_vector_subvector (&plugin_data->iview.vector, 2, 2);
    ebers_moll (gsl_vector_get (plugin_data->v, 2), gsl_vector_get (plugin_data->v, 3), &vv.vector, NULL);
#else
    gsl_multiroot_fdfsolver_set (plugin_data->solver, &plugin_data->nonlinear_system, plugin_data->i);
    do {
      gsl_multiroot_fdfsolver_iterate (plugin_data->solver);
    } while (iter++ < 10 && gsl_blas_dnrm2 (gsl_multiroot_fdfsolver_f (plugin_data->solver)) > 1e-6);
    gsl_vector_memcpy (plugin_data->i, gsl_multiroot_fdfsolver_root (plugin_data->solver));
#endif

    /* x = A * x + B * u + C * i
       y = D * x + E * u + F * i */
    gsl_blas_dgemv (CblasNoTrans, 1, &plugin_data->ABC_DEFview.matrix, plugin_data->xui, 0, plugin_data->x_new_y);
    gsl_vector_memcpy (&plugin_data->xview.vector, &plugin_data->x_newview.vector);

    output[pos] = gsl_vector_get (&plugin_data->yview.vector, 0) / refvolt;
  }
}

static void 
update_coefficients (Crybaby *plugin_data)
{
  /* RvQ = Rv + Q */
  gsl_matrix_memcpy (plugin_data->RvQ, plugin_data->Q);
  *gsl_matrix_ptr (plugin_data->RvQ, 0, 0) += plugin_data->current_hotpotz * potentiometers[0].value;
  *gsl_matrix_ptr (plugin_data->RvQ, 1, 1) += (1 - plugin_data->current_hotpotz) * potentiometers[0].value;
 
  /* invert RvQ */
  int signum;
  gsl_linalg_LU_decomp (plugin_data->RvQ, plugin_data->RvQperm, &signum);
  gsl_linalg_LU_invert (plugin_data->RvQ, plugin_data->RvQperm, plugin_data->RvQinv);

  /* multiply RvQ^-1 with Ux, Uu, Un */
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1, plugin_data->RvQinv, plugin_data->Uxun, 0, plugin_data->RvQinvU);

  /* calculate coefficient matrices */
  gsl_matrix_memcpy (plugin_data->ABC_DEF_GHK, plugin_data->ABC_DEF_GHK0);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, -1, plugin_data->Uleft, plugin_data->RvQinvU, 1, plugin_data->ABC_DEF_GHK);
}

static void 
initialize_state (Crybaby *plugin_data)
{
  /* initialize state to steady-state for given supply voltage */
  int signum, iter;
  gsl_vector_view vv;
  NonlinearEquationParams nl_params;
  gsl_vector *psteady = gsl_vector_alloc (NUM_NONLINEARS);
  gsl_matrix *Ksteady = gsl_matrix_alloc (NUM_NONLINEARS, NUM_NONLINEARS);
  gsl_vector *Bu = gsl_vector_alloc (NUM_STATES);
  gsl_matrix *ImALU = gsl_matrix_alloc (NUM_STATES, NUM_STATES);
  gsl_matrix *ImAinv = gsl_matrix_alloc (NUM_STATES, NUM_STATES);
  gsl_vector *ImAinvBu = gsl_vector_alloc (NUM_STATES);
  gsl_matrix *ImAinvC = gsl_matrix_alloc (NUM_STATES, NUM_NONLINEARS);
  gsl_matrix_view Aview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, 0, 0, NUM_STATES, NUM_STATES);
  gsl_matrix_view Bview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, 0, NUM_STATES, NUM_STATES, NUM_VOLTAGE_SOURCES);
  gsl_matrix_view Cview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, 0, NUM_STATES + NUM_VOLTAGE_SOURCES, NUM_STATES, NUM_NONLINEARS);
  gsl_matrix_view Gview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, NUM_STATES + 1, 0, NUM_NONLINEARS, NUM_STATES);
  gsl_matrix_view Hview = gsl_matrix_submatrix (plugin_data->ABC_DEF_GHK, NUM_STATES + 1, NUM_STATES, NUM_NONLINEARS, NUM_VOLTAGE_SOURCES);

  /* in steady state, we have x' = A * x' + B * u + C * i', which gives
   * x' = (I - A)^-1 * (B * u + C * i'); from that, we obtain the implicit
   * non-linear equation
   * v' = (H + G * (I - A)^-1 * B) * u + K * (I - A)^-1 * C * i'
   * withe the Ebers-Moll-equation for i'(v') */

  /* determine (I - A)^-1 */
  gsl_permutation *perm = gsl_permutation_calloc (NUM_STATES);
  gsl_matrix_set_identity (ImALU);
  gsl_matrix_sub (ImALU, &Aview.matrix);
  gsl_linalg_LU_decomp (ImALU, perm, &signum);
  gsl_linalg_LU_invert (ImALU, perm, ImAinv);
  
  /* calculate (H + G * (I - A)^-1 * B) * u */
  gsl_vector_set (&plugin_data->uview.vector, 0, 0.);
  gsl_vector_set (&plugin_data->uview.vector, 1, SUPPLY_VOLTAGE);
  gsl_blas_dgemv (CblasNoTrans, 1, &Hview.matrix, &plugin_data->uview.vector, 0, psteady);
  gsl_blas_dgemv (CblasNoTrans, 1, &Bview.matrix, &plugin_data->uview.vector, 0, Bu);
  gsl_blas_dgemv (CblasNoTrans, 1, ImAinv, Bu, 0, ImAinvBu);
  gsl_blas_dgemv (CblasNoTrans, 1, &Gview.matrix, ImAinvBu, 1, psteady);
  gsl_vector_free (Bu);
  gsl_vector_free (ImAinvBu);

  /* calculate K * (I - A)^-1 * C */
  gsl_matrix_memcpy (Ksteady, &plugin_data->Kview.matrix);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, ImAinv, &Cview.matrix, 0, ImAinvC);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, &Gview.matrix, ImAinvC, 1, Ksteady);
  gsl_matrix_free (ImAinv);
  gsl_matrix_free (ImAinvC);

  /* solve non-linear equation */
  nl_params.K = Ksteady;
  nl_params.p = psteady;
#if SOLVE_FOR_V
  nl_params.i = gsl_vector_alloc (4);
#else
  nl_params.v = gsl_vector_alloc (4);
#endif
  nl_params.Jt = gsl_matrix_calloc (4, 4);
  plugin_data->nonlinear_system.params = &nl_params;

  iter = 0;
#if SOLVE_FOR_V
  gsl_multiroot_fdfsolver_set (plugin_data->solver, &plugin_data->nonlinear_system, plugin_data->v);
  do {
    gsl_multiroot_fdfsolver_iterate (plugin_data->solver);
  } while (iter++ < 1000 && gsl_blas_dnrm2 (gsl_multiroot_fdfsolver_f (plugin_data->solver)) > 0.0001);
  gsl_vector_memcpy (plugin_data->v, gsl_multiroot_fdfsolver_root (plugin_data->solver));
  vv = gsl_vector_subvector (&plugin_data->iview.vector, 0, 2);
  ebers_moll (gsl_vector_get (plugin_data->v, 0), gsl_vector_get (plugin_data->v, 1), &vv.vector, NULL);
  vv = gsl_vector_subvector (&plugin_data->iview.vector, 2, 2);
  ebers_moll (gsl_vector_get (plugin_data->v, 2), gsl_vector_get (plugin_data->v, 3), &vv.vector, NULL);
#else
  {
    gsl_matrix *KLU = gsl_matrix_alloc (NUM_NONLINEARS, NUM_NONLINEARS);
    gsl_permutation *Kperm = gsl_permutation_calloc (NUM_NONLINEARS);
    gsl_matrix_memcpy (KLU, Ksteady);
    gsl_linalg_LU_decomp (KLU, Kperm, &signum);
    gsl_linalg_LU_solve (KLU, Kperm, psteady, plugin_data->i);
    gsl_vector_scale (plugin_data->i, -1.);
  }
  gsl_multiroot_fdfsolver_set (plugin_data->solver, &plugin_data->nonlinear_system, plugin_data->i);
  do {
    gsl_multiroot_fdfsolver_iterate (plugin_data->solver);
  } while (iter++ < 1000 && gsl_blas_dnrm2 (gsl_multiroot_fdfsolver_f (plugin_data->solver)) > 1e-12);
  gsl_vector_memcpy (plugin_data->i, gsl_multiroot_fdfsolver_root (plugin_data->solver));
#endif

  /* finally, compute steady state */
  gsl_blas_dgemv (CblasNoTrans, 1, &Bview.matrix, &plugin_data->uview.vector, 0, &plugin_data->xview.vector);
  gsl_blas_dgemv (CblasNoTrans, 1, &Cview.matrix, &plugin_data->iview.vector, 1, &plugin_data->xview.vector);
  gsl_linalg_LU_svx (ImALU, perm, &plugin_data->xview.vector);

#if SOLVE_FOR_V
  gsl_vector_free (nl_params.i);
#else
  gsl_vector_free (nl_params.v);
#endif
  gsl_matrix_free (nl_params.Jt);
  gsl_matrix_free (ImALU);
  gsl_matrix_free (Ksteady);
  gsl_vector_free (psteady);
  gsl_permutation_free (perm);
}

#if SOLVE_FOR_V
static int
nonlinear_equation_f (const gsl_vector *v, void *params, gsl_vector *res)
{
  NonlinearEquationParams *nl_params = (NonlinearEquationParams *) params;

  gsl_vector_view vv;

  vv = gsl_vector_subvector (nl_params->i, 0, 2);
  ebers_moll (gsl_vector_get (v, 0), gsl_vector_get (v, 1), &vv.vector, NULL);

  vv = gsl_vector_subvector (nl_params->i, 2, 2);
  ebers_moll (gsl_vector_get (v, 2), gsl_vector_get (v, 3), &vv.vector, NULL);

  gsl_vector_memcpy (res, v);
  gsl_blas_dgemv (CblasNoTrans, 1, nl_params->K, nl_params->i, -1, res);
  gsl_vector_add (res, nl_params->p);

  return GSL_SUCCESS;
}

static int
nonlinear_equation_df (const gsl_vector *v, void *params, gsl_matrix *J)
{
  NonlinearEquationParams *nl_params = (NonlinearEquationParams *) params;

  gsl_matrix_view mv;

  mv = gsl_matrix_submatrix (nl_params->Jt, 0, 0, 2, 2);
  ebers_moll (gsl_vector_get (v, 0), gsl_vector_get (v, 1), NULL, &mv.matrix);

  mv = gsl_matrix_submatrix (nl_params->Jt, 2, 2, 2, 2);
  ebers_moll (gsl_vector_get (v, 2), gsl_vector_get (v, 3), NULL, &mv.matrix);

  gsl_matrix_set_identity (J);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, nl_params->K, nl_params->Jt, -1, J);

  return GSL_SUCCESS;
}

static int
nonlinear_equation_fdf (const gsl_vector *v, void *params, gsl_vector *res, 
			gsl_matrix *J)
{

  gsl_vector_view vv;
  gsl_matrix_view mv;

  NonlinearEquationParams *nl_params = (NonlinearEquationParams *) params;

  vv = gsl_vector_subvector (nl_params->i, 0, 2);
  mv = gsl_matrix_submatrix (nl_params->Jt, 0, 0, 2, 2);
  ebers_moll (gsl_vector_get (v, 0), gsl_vector_get (v, 1), &vv.vector, &mv.matrix);

  vv = gsl_vector_subvector (nl_params->i, 2, 2);
  mv = gsl_matrix_submatrix (nl_params->Jt, 2, 2, 2, 2);
  ebers_moll (gsl_vector_get (v, 2), gsl_vector_get (v, 3), &vv.vector, &mv.matrix);

  gsl_vector_memcpy (res, v);
  gsl_blas_dgemv (CblasNoTrans, 1, nl_params->K, nl_params->i, -1, res);
  gsl_vector_add (res, nl_params->p);

  gsl_matrix_set_identity (J);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, nl_params->K, nl_params->Jt, -1, J);

  return GSL_SUCCESS;
}
#else
static int
nonlinear_equation_f (const gsl_vector *i, void *params, gsl_vector *res)
{
  NonlinearEquationParams *nl_params = (NonlinearEquationParams *) params;

  gsl_vector_view vv;

  gsl_vector_memcpy (nl_params->v, nl_params->p);
  gsl_blas_dgemv (CblasNoTrans, 1, nl_params->K, i, 1, nl_params->v);

  vv = gsl_vector_subvector (res, 0, 2);
  ebers_moll (gsl_vector_get (nl_params->v, 0), gsl_vector_get (nl_params->v, 1), &vv.vector, NULL);

  vv = gsl_vector_subvector (res, 2, 2);
  ebers_moll (gsl_vector_get (nl_params->v, 2), gsl_vector_get (nl_params->v, 3), &vv.vector, NULL);

  gsl_vector_sub (res, i);

  return GSL_SUCCESS;
}

static int
nonlinear_equation_df (const gsl_vector *i, void *params, gsl_matrix *J)
{
  NonlinearEquationParams *nl_params = (NonlinearEquationParams *) params;

  gsl_matrix_view mv;

  gsl_vector_memcpy (nl_params->v, nl_params->p);
  gsl_blas_dgemv (CblasNoTrans, 1, nl_params->K, i, 1, nl_params->v);

  mv = gsl_matrix_submatrix (nl_params->Jt, 0, 0, 2, 2);
  ebers_moll (gsl_vector_get (nl_params->v, 0), gsl_vector_get (nl_params->v, 1), NULL, &mv.matrix);

  mv = gsl_matrix_submatrix (nl_params->Jt, 2, 2, 2, 2);
  ebers_moll (gsl_vector_get (nl_params->v, 2), gsl_vector_get (nl_params->v, 3), NULL, &mv.matrix);

  gsl_matrix_set_identity (J);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, nl_params->Jt, nl_params->K, -1, J);

  return GSL_SUCCESS;
}

static int
nonlinear_equation_fdf (const gsl_vector *i, void *params, gsl_vector *res, 
			gsl_matrix *J)
{
  NonlinearEquationParams *nl_params = (NonlinearEquationParams *) params;

  gsl_vector_view vv;
  gsl_matrix_view mv;

  gsl_vector_memcpy (nl_params->v, nl_params->p);
  gsl_blas_dgemv (CblasNoTrans, 1, nl_params->K, i, 1, nl_params->v);

  vv = gsl_vector_subvector (res, 0, 2);
  mv = gsl_matrix_submatrix (nl_params->Jt, 0, 0, 2, 2);
  ebers_moll (gsl_vector_get (nl_params->v, 0), gsl_vector_get (nl_params->v, 1), &vv.vector, &mv.matrix);

  vv = gsl_vector_subvector (res, 2, 2);
  mv = gsl_matrix_submatrix (nl_params->Jt, 2, 2, 2, 2);
  ebers_moll (gsl_vector_get (nl_params->v, 2), gsl_vector_get (nl_params->v, 3), &vv.vector, &mv.matrix);

  gsl_vector_sub (res, i);

  gsl_matrix_set_identity (J);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, nl_params->Jt, nl_params->K, -1, J);

  return GSL_SUCCESS;
}
#endif

static void
ebers_moll (double vCB, double vCE, gsl_vector *i, gsl_matrix *J)
{
  const double Is = 20.3e-15;
  const double betaF = 1430;
  const double betaR = 4;
  const double Vt = 26e-3;

  double vBE = vCE - vCB;
  double vBC = -vCB;
  double ex1;
  double ex2;
  ex1 = exp(vBE/Vt);
  ex2 = exp(vBC/Vt);
  if (i != NULL) {
    double iB = Is/betaF*(ex1-1)+Is/betaR*(ex2-1);
    double iE = -Is*(ex1-1) + (betaR-1)/betaR*Is*(ex2-1);
    gsl_vector_set (i, 0, iB);
    gsl_vector_set (i, 1, iE);
  }
  if (J != NULL) {
    double diBdvBE = Is/betaF/Vt*ex1;
    double diBdvBC = Is/betaR/Vt*ex2;
    double diEdvBE = -Is/Vt*ex1;
    double diEdvBC = (betaR-1)/betaR*Is/Vt*ex2;
    gsl_matrix_set (J, 0, 0, -diBdvBE - diBdvBC);
    gsl_matrix_set (J, 0, 1, diBdvBE);
    gsl_matrix_set (J, 1, 0, -diEdvBE - diEdvBC);
    gsl_matrix_set (J, 1, 1, diEdvBE);
  }
}

/*static void init()
{
  crybabyDescriptor =
    (LV2_Descriptor *)malloc(sizeof(LV2_Descriptor));

  crybabyDescriptor->URI = CRYBABY_URI;
  crybabyDescriptor->activate = NULL;
  crybabyDescriptor->cleanup = cleanup;
  crybabyDescriptor->connect_port = connectPort;
  crybabyDescriptor->deactivate = NULL;
  crybabyDescriptor->instantiate = instantiate;
  crybabyDescriptor->run = run;
  crybabyDescriptor->extension_data = NULL;
}*/


/*LV2_SYMBOL_EXPORT
const LV2_Descriptor *lv2_descriptor(uint32_t index)
{
  if (!crybabyDescriptor) init();

  switch (index) {
    case 0:
      return crybabyDescriptor;
    default:
      return NULL;
  }
}*/

}
