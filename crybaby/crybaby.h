/* crybaby.c
 *
 * Copyright 2011 Martin Holters
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CRYBABY_H_
#define CRYBABY_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <math.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multiroots.h>

#define SOLVE_FOR_V 1

namespace CrybabySpace {

typedef struct {
  gsl_matrix *K;
  gsl_vector *p;
#if SOLVE_FOR_V
  gsl_vector *i;
#else
  gsl_vector *v;
#endif
  gsl_matrix *Jt;
} NonlinearEquationParams;

/* run-time state information of the plugin; alos includes memory for
 * temporaries to avoid allocation/de-allocation during time-critcal processing
 */
typedef struct {
  /* pointers to I/O port data */
  float *input;
  float *output;
  float *refvolt;
  float *hotpotz;

  /* smoothed potentiometer setting */
  float current_hotpotz;

  double sampling_rate;

  /* input high-pass filter coeffiencts and state */
  double input_a;
  double input_b;
  double input_state;

  gsl_matrix *S0;    /* system matrix without potentiometer */
  gsl_matrix *S0inv; /* its inverse */
  gsl_matrix *Q;     /* constant parts of the coefficient matrices */
  gsl_matrix *Uleft; /* (Z*G*Ux  Uo  Un)^T */
  gsl_matrix *Uxun;  /* (Ux Uu Un) */
  gsl_matrix *ABC_DEF_GHK0; /* (A0 B0 C0; D0 E0 F0; G0 H0 K0) */

  gsl_vector *xui;
  gsl_vector_view xview; 
  gsl_vector_view uview; 
  gsl_vector_view xuview; 
  gsl_vector_view iview; 

  gsl_vector *v;

  gsl_matrix *RvQ;      /* temporaries for coefficient recalculation */
  gsl_permutation *RvQperm;
  gsl_matrix *RvQinv;
  gsl_matrix *RvQinvU;  /* (Rv + Q)^-1 * (Ux Uu Un) */
  gsl_matrix *ABC_DEF_GHK;      /* coefficient matrices */
  gsl_matrix_view Kview; 
  gsl_matrix_view ABC_DEFview; 
  gsl_matrix_view GHview; 

  gsl_vector *p;
  gsl_vector *x_new_y;
  gsl_vector_view x_newview; 
  gsl_vector_view yview; 
  NonlinearEquationParams nl_params;
  gsl_multiroot_fdfsolver *solver;
  gsl_multiroot_function_fdf nonlinear_system;
} Crybaby;

void cleanup(Crybaby *instance);
void connectPort(Crybaby *instance, float*inputptr, float*outputptr, float*refVolt, float*hotPotz);
void addHotPotz(Crybaby *instance, float val);
Crybaby *instantiate (double s_rate);
void run (Crybaby* instance, uint32_t sample_count);

}
#endif
