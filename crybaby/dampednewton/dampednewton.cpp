/* dampednewton.c
 * 
 * Copyright (C) 2011 Martin Holters
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include "dampednewton.h"

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>

/* Simple damped Newton method (rejects uphill steps). Derived from the Newton
 * solver by Brian Gough as contained in the GSL. */

typedef struct {
  double phi;
  double step_size;
  gsl_vector *x_trial;
  gsl_vector *f_trial;
  gsl_vector *d;
  gsl_matrix *J_trial;
  gsl_matrix *lu;
  gsl_permutation *permutation;
} damped_newton_state_t;

static int damped_newton_alloc (void *vstate, size_t n);
static int damped_newton_set (void *vstate, gsl_multiroot_function_fdf *fdf,
                              gsl_vector *x, gsl_vector *f, gsl_matrix *J,
                              gsl_vector *dx);
static int damped_newton_iterate (void *vstate, gsl_multiroot_function_fdf *fdf,
                                  gsl_vector *x, gsl_vector *f, gsl_matrix *J,
                                  gsl_vector *dx);
static void damped_newton_free (void *vstate);

static int
damped_newton_alloc (void *vstate, size_t n)
{
  damped_newton_state_t *state = (damped_newton_state_t *) vstate;
  gsl_vector *d, *x_trial, *f_trial;
  gsl_permutation *p;
  gsl_matrix *m, *J_trial;

  m = gsl_matrix_calloc (n,n);
  
  if (m == 0) {
    GSL_ERROR ("failed to allocate space for lu", GSL_ENOMEM);
  }

  state->lu = m ;

  J_trial = gsl_matrix_calloc (n,n);
  if (J_trial == 0) {
    gsl_matrix_free(m);
    GSL_ERROR ("failed to allocate space for J_trial", GSL_ENOMEM);
  }
  state->J_trial = J_trial ;

  p = gsl_permutation_calloc (n);

  if (p == 0) {
    gsl_matrix_free(m);
    gsl_matrix_free(J_trial);

    GSL_ERROR ("failed to allocate space for permutation", GSL_ENOMEM);
  }

  state->permutation = p ;

  d = gsl_vector_calloc (n);

  if (d == 0) {
    gsl_permutation_free(p);
    gsl_matrix_free(m);
    gsl_matrix_free(J_trial);

    GSL_ERROR ("failed to allocate space for d", GSL_ENOMEM);
  }

  state->d = d;

  x_trial = gsl_vector_calloc (n);

  if (x_trial == 0) {
    gsl_vector_free(d);
    gsl_permutation_free(p);
    gsl_matrix_free(m);
    gsl_matrix_free(J_trial);

    GSL_ERROR ("failed to allocate space for x_trial", GSL_ENOMEM);
  }

  state->x_trial = x_trial;

  f_trial = gsl_vector_calloc (n);

  if (f_trial == 0) {
    gsl_vector_free(x_trial);
    gsl_vector_free(d);
    gsl_permutation_free(p);
    gsl_matrix_free(m);
    gsl_matrix_free(J_trial);

    GSL_ERROR ("failed to allocate space for f_trial", GSL_ENOMEM);
  }

  state->f_trial = f_trial;

  return GSL_SUCCESS;
}


static int
damped_newton_set (void *vstate, gsl_multiroot_function_fdf *FDF, gsl_vector *x,
                   gsl_vector *f, gsl_matrix *J, gsl_vector *dx)
{
  int signum;
  damped_newton_state_t * state = (damped_newton_state_t *) vstate;

  GSL_MULTIROOT_FN_EVAL_F_DF (FDF, x, f, J);

  gsl_matrix_memcpy (state->lu, J);
  gsl_linalg_LU_decomp (state->lu, state->permutation, &signum);
  gsl_linalg_LU_solve (state->lu, state->permutation, f, state->d);

  gsl_vector_set_zero (dx);

  state->phi = gsl_blas_dnrm2 (f);

  state->step_size = 1.;

  return GSL_SUCCESS;
}

static int
damped_newton_iterate (void *vstate, gsl_multiroot_function_fdf *fdf,
                       gsl_vector *x, gsl_vector *f, gsl_matrix *J,
                       gsl_vector *dx)
{
  damped_newton_state_t * state = (damped_newton_state_t *) vstate;
  
  int signum, status;
  double phi0, phi1;

  phi0 = state->phi;

  gsl_vector_memcpy (state->x_trial, x);
  gsl_blas_daxpy (-state->step_size, state->d, state->x_trial);
  
  status = GSL_MULTIROOT_FN_EVAL_F_DF (fdf, state->x_trial, state->f_trial, state->J_trial);
  if (status != GSL_SUCCESS) {
    return GSL_EBADFUNC;
  }
  
  phi1 = gsl_blas_dnrm2  (state->f_trial);

  if (!(phi1 < phi0) && state->step_size > GSL_DBL_EPSILON)  {
    /* full step goes uphill, take a reduced step next time */

    state->step_size *= .5;
      
    gsl_vector_set_zero (dx);
    return GSL_SUCCESS;
  }

  /* copy x_trial into x */

  gsl_vector_memcpy (x, state->x_trial);
  gsl_vector_memcpy (f, state->f_trial);
  gsl_matrix_memcpy (J, state->J_trial);

  gsl_vector_memcpy (dx, state->d);
  gsl_blas_dscal (-state->step_size, dx);

  gsl_matrix_memcpy (state->lu, J);
  gsl_linalg_LU_decomp (state->lu, state->permutation, &signum);
  gsl_linalg_LU_solve (state->lu, state->permutation, f, state->d);

  state->step_size = 1.;

  state->phi = phi1;

  return GSL_SUCCESS;
}


static void
damped_newton_free (void *vstate)
{
  damped_newton_state_t * state = (damped_newton_state_t *) vstate;

  gsl_vector_free(state->d);
  gsl_vector_free(state->x_trial);
  gsl_vector_free(state->f_trial);
  gsl_matrix_free(state->lu);
  gsl_matrix_free(state->J_trial);
  gsl_permutation_free(state->permutation);
}


static const gsl_multiroot_fdfsolver_type damped_newton_type = {
  "damped_newton",                             /* name */
  sizeof (damped_newton_state_t),
  &damped_newton_alloc,
  &damped_newton_set,
  &damped_newton_iterate,
  &damped_newton_free
};

const gsl_multiroot_fdfsolver_type *
multiroot_fdfsolver_damped_newton = &damped_newton_type;
