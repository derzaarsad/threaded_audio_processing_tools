/* dampednewton.h
 * 
 * Copyright (C) 2011 Martin Holters
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DAMPEDNEWTON_H__
#define __DAMPEDNEWTON_H__ 1

#include <gsl/gsl_multiroots.h>

extern const gsl_multiroot_fdfsolver_type  * multiroot_fdfsolver_damped_newton;

#endif /* __DAMPEDNEWTON_H__ */
