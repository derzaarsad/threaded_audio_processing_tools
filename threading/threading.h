// --------------------------------------------------------------------------
//  author:  Derza Arsad
// --------------------------------------------------------------------------

#ifndef THREADING_H_
#define THREADING_H_ 1
#include <tbb/concurrent_queue.h>
#include <thread>
#include <atomic>
#include <iostream>
#include <system_error>
#define BUFFER_SIZE 512
#define SAMPLE_RATE 44100

class ProcessedBuf
{
private:

public:
    ProcessedBuf();
    ~ProcessedBuf();
    float *bufptr;
    void cloneFrom(const float *bufinput);
    void cloneTo(float *bufoutput);
};

class MemoryAlloc
{
private:
    const int queue_length = 5;
public:
    tbb::concurrent_bounded_queue<ProcessedBuf> queueFree;
    tbb::concurrent_bounded_queue<ProcessedBuf> queuePending;
    void generateQueue(void);
    MemoryAlloc(){}
    ~MemoryAlloc(){}
    void catchBuf(const float *bufptr);
};

class ThreadedProcess
{
private:
    std::unique_ptr<MemoryAlloc> alloc;
    std::thread trackingThread;
    std::atomic_bool stop_tracking_thread;
    ProcessedBuf current_buf;
public:
    ThreadedProcess();
    ~ThreadedProcess();
    void start(void);
    void stop(void);
    void run(void);
    bool isRunning() const { return trackingThread.joinable(); }
    void catchBuf(const float *bufptr);
    float *bufout;

    // Crybaby
    bool crybaby_used = false;
};

#endif
