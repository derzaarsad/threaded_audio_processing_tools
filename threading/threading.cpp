// --------------------------------------------------------------------------
//  author:  Derza Arsad
// --------------------------------------------------------------------------

#include "threading.h"
#include <tbb/concurrent_queue.h>
#include <thread>
#include <atomic>
#include <iostream>
#include <system_error>
#include "../audio/audio.h"
#include "../crybaby/crybaby.h"

ProcessedBuf::ProcessedBuf()
{
    bufptr = new float[BUFFER_SIZE];
}

ProcessedBuf::~ProcessedBuf(){}

void ProcessedBuf::cloneFrom(const float *bufinput)
{
    for(int i=0; i<BUFFER_SIZE; i++) bufptr[i] = bufinput[i];
}

void ProcessedBuf::cloneTo(float *bufoutput)
{
    for(int i=0; i<BUFFER_SIZE; i++) bufoutput[i] = bufptr[i];
}

void MemoryAlloc::generateQueue()
{
    queueFree.clear();
    queueFree.set_capacity(queue_length);
    queuePending.clear();
    queuePending.set_capacity(queue_length);

    for (int i = 0; i < queue_length; i++)
    {
        ProcessedBuf bufAlloc;
        queueFree.push(bufAlloc);
    }
}

void MemoryAlloc::catchBuf(const float *bufptr)
{
    ProcessedBuf bufAlloc;
    if (!queueFree.try_pop(bufAlloc)) { return; }

    // clone
    bufAlloc.cloneFrom(bufptr);

    queuePending.push(bufAlloc);
}

ThreadedProcess::ThreadedProcess()
{
    alloc = std::unique_ptr<MemoryAlloc>(new MemoryAlloc());
    bufout = new float[BUFFER_SIZE];
    for(int i=0; i<BUFFER_SIZE; i++) bufout[i] = 0.0;
}

ThreadedProcess::~ThreadedProcess() {}

void ThreadedProcess::catchBuf(const float *bufptr)
{
    // Accept data only when the system is already running.
    if (!isRunning())
    {
        return;
    }
    alloc->catchBuf(bufptr);
}

void ThreadedProcess::start()
{
    stop();

    stop_tracking_thread = false;

    try {
      trackingThread = std::thread(&ThreadedProcess::run, this);
    }
    catch (std::system_error& e) {
      std::cerr << std::endl << e.what() << std::endl;
    }
}

void ThreadedProcess::stop()
{
    stop_tracking_thread = true;

    if (isRunning()) {
      try {
        trackingThread.join();
      } catch (std::system_error& e) {
        std::cerr << "Error - Thread exception. " << e.what() << std::endl;
      }
    } else
    {
      std::cerr << "Warning - the thread was already stopped." << std::endl;
    }
}

void ThreadedProcess::run(void)
{
    alloc->generateQueue();

    // ---- Gerüst für Playback ----------------------------------------------------

    snd_pcm_t *playback;
    //	size_t blocksize;										// Anzahl samples pro block
    //	size_t fsize;											// byte pro sample

    // ---- Playback Parameter----------------------------------------------------

    audio_param_t param_out;
    param_out.name = "default";
    param_out.stream = SND_PCM_STREAM_PLAYBACK;
    param_out.access = SND_PCM_ACCESS_RW_INTERLEAVED;
    param_out.format = SND_PCM_FORMAT_FLOAT_LE;								// auf Big Endian umgestellt
    param_out.channel = 1;													// mono
    param_out.rate = SAMPLE_RATE;											// Abtastrate

    // Playback init
    audio_init(&playback, &param_out);
    int ret;

    // Crybaby
    float *refVolt;
    float refVoltref = 1.0;
    refVolt = &refVoltref;
    float *hotPotz;
    float hotPotzref = 0.5;
    hotPotz = &hotPotzref;
    CrybabySpace::Crybaby *plugin_data= CrybabySpace::instantiate (SAMPLE_RATE);

    // Enter loop
    while (!stop_tracking_thread)
    {
        // Check data availability
        // The scanner should only work if there is valid data in the queue
        if (!alloc->queuePending.try_pop(current_buf))
        {
            // This is for freeing the processor.
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            continue;
        }

        // some process
        if(crybaby_used == true)
        {
            CrybabySpace::connectPort(plugin_data, current_buf.bufptr, bufout, refVolt, hotPotz);
            CrybabySpace::run(plugin_data, BUFFER_SIZE);
        }
        else current_buf.cloneTo(bufout);

        ret = snd_pcm_writei (playback, bufout, BUFFER_SIZE);
        if (ret == -EPIPE)
        {
            /* EPIPE means underrun */
            fprintf(stderr, "underrun occurred\n");
            snd_pcm_prepare(playback);
        }
        else if (ret < 0)
        {
            fprintf(stderr, "error from writei: %s\n", snd_strerror(ret));
        }
        else if (ret != (int)BUFFER_SIZE)
        {
            fprintf(stderr,"short write, write %d frames\n", ret);
        }
        alloc->queueFree.try_push(current_buf);

        if(crybaby_used == true) CrybabySpace::addHotPotz(plugin_data, 0.01);
    }
    ret = snd_pcm_close(playback);										// close playback
}
