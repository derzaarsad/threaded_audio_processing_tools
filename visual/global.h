// --------------------------------------------------------------------------
//  Copyright © 2015  Christian Steiger  -  All rights reserved
// --------------------------------------------------------------------------
#ifndef INFOTECH_AUFGABE_1_BLOCK_GLOBAL_H_
#define INFOTECH_AUFGABE_1_BLOCK_GLOBAL_H_ 1

#include <alsa/asoundlib.h>

struct param
{
	      const char *name;
	snd_pcm_stream_t  stream;
	snd_pcm_access_t  access;
	snd_pcm_format_t  format;
	    unsigned int  channel;
	    unsigned int  rate;
};

typedef struct param param_t;

#endif
