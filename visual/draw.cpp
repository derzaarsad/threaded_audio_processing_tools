// --------------------------------------------------------------------------
//  Copyright © 2015  Christian Steiger  -  All rights reserved
// --------------------------------------------------------------------------

#ifndef _GNU_SOURCE
#define _GNU_SOURCE    // Nötig für asprintf()
#endif

#include <stdio.h>     // asprintf()
#include <stdlib.h>    // abort()
#include <unistd.h>    // usleep
#include <math.h>      // tanh
#include <GLES/gl.h>   // OpenGL ES 1.1 Datentypen und Funktionen
#include <iostream>

#include "global.h"
#include "gles.h"
#include "draw.h"
using namespace std;

// --------------------------------------------------------------------------
//  Oberfläche in Endlosschleife zeichnen
// --------------------------------------------------------------------------
void* draw_main(void *pdata)
{
	draw_data_t *data = (draw_data_t *)pdata;

	// OpenGL ES initialisieren
	struct opengles opengles;
	glesInitialize(&opengles);

	GLfloat *vertices = (GLfloat *)malloc((sizeof(GLfloat) * data->blocksize * 2));	// 2* wg x,y

	do
	{
		// Buffer2Vertices
		GLfloat x = -2.0f;
		GLfloat stepx = 4.0f / (GLfloat)data->blocksize;				// x-Achse normieren
		GLfloat stepy = 1.0f; // / 32768.0f;								// +-1 Skala für OpenGL

		size_t i;
		for (i = 0; i < data->blocksize; i++)
		{
			int o = i * 2;												// * 2 wg. x, y
			vertices[o + 0] = x;
			vertices[o + 1] = ( data->bufptr[i]) * stepy;

//			cout << "x  "<< x << " y  "<< stepy << endl;
			x += stepx;
		}

		// Framebuffer löschen.
		glClear(GL_COLOR_BUFFER_BIT);

		// OpenGL mitteilen, dass Vertex- und Texturdaten geschrieben werden.
		glEnableClientState(GL_VERTEX_ARRAY);

		// Dashboard-Vertex-Koordinaten übergeben.
		glVertexPointer(2, GL_FLOAT, 0, vertices);

		// Objekt zeichnen.
		glDrawArrays(GL_LINE_STRIP, 0, data->blocksize);

		// Vertex- und Texturdaten wurden geschrieben.
		glDisableClientState(GL_VERTEX_ARRAY);

		// Das gezeichnete Bild sichtbar machen.
		glesDraw(&opengles);

		// 40ms warten um CPU-Last zu verringern.
		usleep(40 * 1000);
	}
	while(glesRun(&opengles));

	// OpenGL ES Ressourcen freigeben.
	glesDestroy(&opengles);

    data->isDestroyed = true;

	return 0;
}
