// --------------------------------------------------------------------------
//  Copyright © 2015  Christian Steiger  -  All rights reserved
// --------------------------------------------------------------------------

#ifndef INFOTECH_AUFGABE_1_BLOCK_DRAW_H_
#define INFOTECH_AUFGABE_1_BLOCK_DRAW_H_ 1

#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <GLES/gl.h>


struct draw_data
{
	float *bufptr;
	size_t blocksize;
    bool isDestroyed;
};
typedef struct draw_data draw_data_t;

// Funktionsprototypen
void* draw_main(void *);

#endif
