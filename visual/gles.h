// --------------------------------------------------------------------------
//  Copyright © 2014  Christian Steiger  -  All rights reserved
// --------------------------------------------------------------------------

// Sicherstellen, dass die Header-Datei nur einmal eingebunden wird.
#ifndef INFOTECH_AUFGABE_1_BLOCK_GLES_H_
#define INFOTECH_AUFGABE_1_BLOCK_GLES_H_ 1

#include <EGL/egl.h>   // EGL Datentypen und Funktionen
#include <GLES/gl.h>   // OpenGL ES 1.1 Datentypen und Funktionen
#include <X11/Xlib.h>  // XLib Datentypen und Funktionen

struct opengles
{
	// EGL
	EGLDisplay  display;
	EGLSurface  surface;
	EGLContext  context;

	// Xlib
	Display    *xlib_display;
	Window      xlib_window;

	// Dimensionen des Fensters
	uint16_t    width;
	uint16_t    height;
	float       ratio;
};

// Funktionsprototyp für can_open-Funktion.
int glesInitialize(struct opengles *opengles);
int glesDraw(struct opengles *opengles);
int glesRun(struct opengles *gles);
int glesDestroy(struct opengles *opengles);

#endif
